uint8_t theAnalogPins[5] = {A0, A1, A2, A3, A4};
boolean buttonState[5] = {LOW, LOW, LOW, LOW, LOW};
boolean lastButtonState[5] = {LOW, LOW, LOW, LOW, LOW};
long lastReading[5];
long intervalSince2Changes[5];

void setup() {
  for (int i = 0; i < 5; i++)
  {
    pinMode(theAnalogPins[i], INPUT);
  }
  Serial.begin(9600);
}


void loop() {
  delay(50);
  // Grab values
  for (int i = 0; i < 5; i++)
  {
    buttonState[i] = digitalRead(theAnalogPins[i]);
    if (buttonState[i] != lastButtonState[i])
    {
      long intervalSince2Changes = millis() - lastReading[i];
      Serial.print(intervalSince2Changes);
      Serial.print(" ");
      lastButtonState[i] = buttonState[i];
    }
    lastReading[i] = millis();
    
  }
  Serial.println();
  //Serial.write(i + 1);
}



