/*
  AnalogReadSerial
  Reads an analog input on pin 0, prints the result to the serial monitor.
  Attach the center pin of a potentiometer to pin A0, and the outside pins to +5V and ground.

 This example code is in the public domain.
 */
int shake1 = 0;
int swing = 1;

uint8_t theAnalogPins[5] = {A0, A1, A2, A3, A4};
int mySensVals[5] = {0, 0, 0, 0, 0};
int mySensVals2[5] = {0, 0, 0, 0, 0};
int myLastSensVals[5] = {0, 0, 0, 0, 0};
int shake[5] = {0, 0, 0, 0, 0};

void setup() {
  Serial.begin(9600);
}


void loop() {

  // Grab values
  for (int i = 0; i < 5; i++)
  {
    mySensVals[i] = digitalRead(theAnalogPins[i]);
    //Serial.print(mySensVals[i]);

  }
  // Wait a little bit
  delay(80);
  //Serial.println();

  // ReRead Values
  for (int i = 0; i < 5; i++)
  {
    mySensVals2[i] = digitalRead(theAnalogPins[i]);
    //Serial.print(mySensVals2[i]);
  }
  //delay(40);
  //Serial.println();
  //Serial.println();
  

  for (int i = 0; i < 5; i++)
  {
    if (mySensVals[i] != mySensVals2[i] && mySensVals[i] != myLastSensVals[i]) 
    {
      shake[i]++;
      myLastSensVals[i] = mySensVals[i];
    }
  
  if (shake[i] > 5)
  {
    Serial.println(i + 1);
    shake[i] = 0;
  }
  
}
}



void listenToBook(uint8_t theAnalogPin)
{
  long theStart = millis();

  int sensorValue = digitalRead(theAnalogPin);
  delay(40);        // delay in between reads for stability
  int sensorValue2 = digitalRead(theAnalogPin);
  while (millis() - theStart < 80)
  {
    switch (swing) {
      case 1:
        if (sensorValue == 1 && sensorValue2 == 0)
        {
          shake1++;
          swing = 2;
        }
        break;
      case 2:
        if (sensorValue == 0 && sensorValue2 == 1)
        {
          shake1++;
          swing = 1;
        }
        break;
    }
    //Serial.println(shake1);
    if (shake1 > 5)
    {
      Serial.println(theAnalogPin);
      shake1 = 0;
      break;
    }
  }
}



