uint8_t theAnalogPins[5] = {A0, A1, A2, A3, A4};
const int numReadings = 10;
int mySensVals[5][numReadings];
int readIndex = 0; 
int total[5];                  // the running total
int average[5];                // the average
int seuil = 25;


void setup() {
  for (int i = 0; i < 5; i++)
  {
    pinMode(theAnalogPins[i], INPUT);
  }
  Serial.begin(9600);
}


void loop() {
  //delay(50);
  // Grab values
  for (int i = 0; i < 5; i++)
  {
    total[i] = total[i] - mySensVals[i][readIndex];
    mySensVals[i][readIndex] = TP_init(i);
    total[i] = total[i] + mySensVals[i][readIndex];
    readIndex = readIndex + 1;
    if (readIndex >= numReadings) readIndex = 0;
    average[i] = total[i] / numReadings;
    //Serial.print(average[i]);
    //Serial.print(" ");
    if (average[i] > seuil)
    {
      //RAZ
      for (int j = 0; j<numReadings; j++)
      {
        mySensVals[i][j] = 0;
        total[i] = 0;
        average[i] = 0;
      }
      
      //Serial.println();
      Serial.write(i + 1);
      
    }
    
  }
 
}



int TP_init(uint8_t pin) {
  delay(10);
  int measurement = pulseIn (theAnalogPins[pin], HIGH, 200000 ); //wait for the pin to get HIGH and returns measurement
  return measurement;
}



