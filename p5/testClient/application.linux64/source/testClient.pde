import processing.io.*;

boolean ledOn = false;

void setup() {
  GPIO.pinMode(9, GPIO.INPUT);
  GPIO.pinMode(2, GPIO.OUTPUT);
}

void draw(){
 if (GPIO.digitalRead(9) == GPIO.HIGH) {
    print ("button");
    GPIO.digitalWrite(2, GPIO.HIGH);
 } else {
    GPIO.digitalWrite(2, GPIO.LOW);
 }
}