import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.io.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Processing_Pi_GPIO extends PApplet {


boolean ledOn = false;

public void setup() {
  GPIO.pinMode(23, GPIO.OUTPUT);

  // On the Raspberry Pi, GPIO 4 is pin 7 on the pin header,
  // located on the fourth row, above one of the ground pins

  frameRate(0.5f);
}

public void draw() {
  ledOn = !ledOn;
  if (ledOn) {
    GPIO.digitalWrite(23, GPIO.LOW);
    fill(204);
  } else {
    GPIO.digitalWrite(23, GPIO.HIGH);
    fill(255);
  }
  stroke(255);
  ellipse(width/2, height/2, width*0.75f, height*0.75f);
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Processing_Pi_GPIO" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
