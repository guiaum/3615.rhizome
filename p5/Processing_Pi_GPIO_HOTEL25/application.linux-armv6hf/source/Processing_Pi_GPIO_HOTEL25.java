import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.io.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Processing_Pi_GPIO_HOTEL25 extends PApplet {


boolean ledOn = false;

public void setup() {
  GPIO.pinMode(18, GPIO.OUTPUT);
  GPIO.pinMode(22, GPIO.OUTPUT);
  GPIO.pinMode(23, GPIO.OUTPUT);
  GPIO.pinMode(24, GPIO.OUTPUT);
  GPIO.pinMode(27, GPIO.OUTPUT);

  // On the Raspberry Pi, GPIO 4 is pin 7 on the pin header,
  // located on the fourth row, above one of the ground pins

  frameRate(0.5f);
}

public void draw() {

}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "Processing_Pi_GPIO_HOTEL25" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
