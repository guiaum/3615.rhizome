import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import websockets.*; 
import processing.serial.*; 
import processing.io.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class rhizome_client extends PApplet {





WebsocketClient wsc;

int val;
Serial myPort;
String valToString;
String[] rawid;
String id;
boolean isShaking = false;
int motorPin;

public void setup() {
  
  rawid = loadStrings("id.txt");
  id = rawid[0];

  wsc= new WebsocketClient(this, "ws://79.137.36.84:8025/john");
  wsc.sendMessage("started - "+id);

  // Main Raspi, with 5 trees.
  if (id.equals("6"))
  {
    GPIO.pinMode(18, GPIO.OUTPUT);
    GPIO.pinMode(22, GPIO.OUTPUT);
    GPIO.pinMode(23, GPIO.OUTPUT);
    GPIO.pinMode(24, GPIO.OUTPUT);
    GPIO.pinMode(27, GPIO.OUTPUT);

    //ARDUINO
    //println (Serial.list());
    String portName = Serial.list()[2];
    myPort = new Serial(this, portName, 9600);
  } else if (id.equals("0")) {
    // test, from other computer
  } else {
    // in the library, only 1 tree
    GPIO.pinMode(23, GPIO.OUTPUT);
    motorPin = 23;

    //ARDUINO
    //println (Serial.list());
    try {
      String portName = Serial.list()[2];
      myPort = new Serial(this, portName, 9600);
    }
    catch (Exception e)
    {
      println(e);
    }
  }

  motorOff(motorPin);
}

public void draw() {
  background(0);
  /*
  try 
  {
    if ( myPort.available() > 0) {  // If data is available,

      val = myPort.read();         // read it and store it in val
      print(val);
      valToString = str(val);
      wsc.sendMessage(valToString);
    }
  }
  catch (Exception e)
  {
    println (e);
  }
  */
}


public void webSocketEvent(String msg) {
  println(msg);
  if (isShaking == false)
  {
    if ( msg.charAt(0) == 'b')
    {
      if (msg.charAt(1)==(id.charAt(0)))
      {
        motorOn(motorPin);
        delay(1000);
        motorOff(motorPin);
      }
    }
  }
}

public void motorOn(int pin)
{
  isShaking = true;
  GPIO.digitalWrite(pin, GPIO.HIGH);
}

public void motorOff(int pin)
{
  isShaking = false;
  GPIO.digitalWrite(pin, GPIO.LOW);
}

  public void settings() {  size(200, 200); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "rhizome_client" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
