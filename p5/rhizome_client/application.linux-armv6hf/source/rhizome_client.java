import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import websockets.*; 
import processing.serial.*; 
import processing.io.*; 
import org.multiply.processing.TimedEventGenerator; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class rhizome_client extends PApplet {

/*
id 1 -> St Vit
 id 2 -> Baume les dames
 id 3 -> Pouilley
 id 4 -> Valdahon
 id 5 -> Isle Doubs
 --
 id h -> h\u00f4tel du d\u00e9partement, avec 5 plantes
 avec les plantes 6, 7, 8, 9, 0
 // Adresse MAC: B8:27:EB:B2:88:26
 // Adresse IP du serveur distant (websocket): 79.137.36.84:8025
 */

//Rediriger port 60250 vers 8080

// G\u00e9rer les prestataires 
// Fiabliser capteurs
// Envoyer adresse IP WAN
// g\u00e9rer video
/*
sudo uv4l --auto-video_nr --driver raspicam --encoding h264 --width 200 --height 300 --framerate 30
// Ca marche bien
*/
// G\u00e9rer d\u00e9connections
// Cr\u00e9er page web (x2)


/*
onError(SocketTimeoutException: Timeout on Read)
java.net.SocketTimeoutException: Timeout on Read
  at org.eclipse.jetty.websocket.common.io.AbstractWebSocketConnection.onReadTimeout(AbstractWebSocketConnection.java:576)
  at org.eclipse.jetty.io.AbstractConnection.onFillInterestedFailed(AbstractConnection.java:162)
  at org.eclipse.jetty.websocket.common.io.AbstractWebSocketConnection.onFillInterestedFailed(AbstractWebSocketConnection.java:522)
  at org.eclipse.jetty.io.AbstractConnection$ReadCallback.failed(AbstractConnection.java:267)
  at org.eclipse.jetty.io.FillInterest.onFail(FillInterest.java:125)
  at org.eclipse.jetty.io.AbstractEndPoint.onIdleExpired(AbstractEndPoint.java:163)
  at org.eclipse.jetty.io.IdleTimeout.checkIdleTimeout(IdleTimeout.java:166)
  at org.eclipse.jetty.io.IdleTimeout$1.run(IdleTimeout.java:50)
  at java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:511)
  at java.util.concurrent.FutureTask.run(FutureTask.java:266)
  at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.access$201(ScheduledThreadPoolExecutor.java:180)
  at java.util.concurrent.ScheduledThreadPoolExecutor$ScheduledFutureTask.run(ScheduledThreadPoolExecutor.java:293)
  at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
  at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
  at java.lang.Thread.run(Thread.java:745)
*/






WebsocketClient wsc;
private TimedEventGenerator motorActionSolo;
private TimedEventGenerator motorAction0;
private TimedEventGenerator motorAction6;
private TimedEventGenerator motorAction7;
private TimedEventGenerator motorAction8;
private TimedEventGenerator motorAction9;

private TimedEventGenerator ping;

int val;
Serial myPort;
String valToString;
String[] rawid;
String id;
boolean isShaking = false;
int motorPin;
boolean USBAvailable;
IntDict  shaking;

public void setup() {
  
  rawid = loadStrings("id.txt");
  id = rawid[0];

  wsc= new WebsocketClient(this, "ws://79.137.36.84:8025/john");
  wsc.sendMessage("started - "+id);
  
  ping =new TimedEventGenerator(this, "pingServer", true);
  ping.setIntervalMs(60000);
    

  shaking = new IntDict();
  // Main Raspi, with 5 trees.
  if (id.equals("h"))
  {
    GPIO.pinMode(18, GPIO.OUTPUT);
    GPIO.pinMode(22, GPIO.OUTPUT);
    GPIO.pinMode(23, GPIO.OUTPUT);
    GPIO.pinMode(24, GPIO.OUTPUT);
    GPIO.pinMode(27, GPIO.OUTPUT);
    // Pour stocker l'\u00e9tat vibrant des plantes et pouvoir les faires jouer en m\u00eame temps.
    shaking.set("6", 0);
    shaking.set("7", 0);
    shaking.set("8", 0);
    shaking.set("9", 0);
    shaking.set("0", 0);
    
    motorAction0 =new TimedEventGenerator(this, "motorOff0", false);
    motorAction0.setIntervalMs(1000);
    
    motorAction6 =new TimedEventGenerator(this, "motorOff6", false);
    motorAction6.setIntervalMs(1000);
    
    motorAction7 =new TimedEventGenerator(this, "motorOff7", false);
    motorAction7.setIntervalMs(1000);
    
    motorAction8 =new TimedEventGenerator(this, "motorOff8", false);
    motorAction8.setIntervalMs(1000);
    
    motorAction9 =new TimedEventGenerator(this, "motorOff9", false);
    motorAction9.setIntervalMs(1000);
  } else {
    // in the library, only 1 tree
    GPIO.pinMode(23, GPIO.OUTPUT);
    motorPin = 23;
    shaking.set(id, 0);
    motorActionSolo =new TimedEventGenerator(this, "motorOff", false);
    motorActionSolo.setIntervalMs(1000);
  }

  //ARDUINO
  //println (Serial.list());
  try {
    String portName = Serial.list()[2];
    myPort = new Serial(this, portName, 9600);
    USBAvailable = true;
  }
  catch (Exception e)
  {
    println(e);
    USBAvailable = false;
  }
}

public void draw() {
  background(0);

  if (USBAvailable)
  {
    if ( myPort.available() > 0) {  // If data is available,
      val = myPort.read();         // read it and store it in val
      print(val);
      valToString = str(val);
      try
      {
        wsc.sendMessage(valToString);
      }catch (Exception e)
      {
       println (e); 
      }
      
    }
  }
}


public void webSocketEvent(String msg) {
  println(msg);

    // Message qui vient depuis le serveur
    if ( msg.charAt(0) == 'b')
    {
      // m\u00eame id que moi
      if (msg.charAt(1)==(id.charAt(0)))
      {
        //activation du moteur s'il n'est pas en train de vibrer
        if( shaking.get(id) == 0)
        {
          shaking.set(id,1);
          GPIO.digitalWrite(23, GPIO.HIGH);
          motorActionSolo.setEnabled(true);
        }
      } else if (id.equals("h")) // si je suis \u00e0 l'h\u00f4tel du d\u00e9partement
      {
        // J'active chaque arbre s\u00e9par\u00e9ment
        if (msg.charAt(1)=='6' && shaking.get("6") == 0)
        {
          shaking.set("6",1);
          GPIO.digitalWrite(18, GPIO.HIGH);
          motorAction6.setEnabled(true);
        } else if (msg.charAt(1)=='7' && shaking.get("7") == 0)
        {
          shaking.set("7",1);
          GPIO.digitalWrite(22, GPIO.HIGH);
          motorAction7.setEnabled(true);
        } else if (msg.charAt(1)=='8' && shaking.get("8") == 0)
        {
          shaking.set("8",1);
          GPIO.digitalWrite(23, GPIO.HIGH);
          motorAction8.setEnabled(true);
        } else if (msg.charAt(1)=='9' && shaking.get("9") == 0)
        {
          shaking.set("9",1);
          GPIO.digitalWrite(24, GPIO.HIGH);
          motorAction9.setEnabled(true);
        } else if (msg.charAt(1)=='0' && shaking.get("0") == 0)
        {
          shaking.set("0",1);
          GPIO.digitalWrite(27, GPIO.HIGH);
          motorAction0.setEnabled(true);
        }
      }
    }
  }


public void motorOff()
{
  shaking.set(id,0);
  GPIO.digitalWrite(motorPin, GPIO.LOW);
  motorActionSolo.setEnabled(false);
}

public void motorOff6()
{
  shaking.set("6",0);
  GPIO.digitalWrite(18, GPIO.LOW);
  motorAction6.setEnabled(false);
}

public void motorOff7()
{
  shaking.set("7",0);
  GPIO.digitalWrite(22, GPIO.LOW);
  motorAction7.setEnabled(false);
}

public void motorOff8()
{
  shaking.set("8",0);
  GPIO.digitalWrite(23, GPIO.LOW);
  motorAction8.setEnabled(false);
}

public void motorOff9()
{
  shaking.set("9",0);
  GPIO.digitalWrite(24, GPIO.LOW);
  motorAction9.setEnabled(false);
}

public void motorOff0()
{
  shaking.set("0",0);
  GPIO.digitalWrite(27, GPIO.LOW);
  motorAction0.setEnabled(false);
}

public void pingServer()
{
  try
      {
        wsc.sendMessage("ping");
      }catch (Exception e)
      {
       println (e); 
      }
}

  public void settings() {  size(200, 200); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "rhizome_client" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
