import websockets.*;
import processing.serial.*;
import processing.io.*;

WebsocketClient wsc;

int val;
Serial myPort;
String valToString;
String[] rawid;
String id;
boolean isShaking = false;
int motorPin;

void setup() {
  size(200, 200);
  rawid = loadStrings("id.txt");
  id = rawid[0];

  wsc= new WebsocketClient(this, "ws://79.137.36.84:8025/john");
  wsc.sendMessage("started - "+id);

  // Main Raspi, with 5 trees.
  if (id.equals("6"))
  {
    GPIO.pinMode(18, GPIO.OUTPUT);
    GPIO.pinMode(22, GPIO.OUTPUT);
    GPIO.pinMode(23, GPIO.OUTPUT);
    GPIO.pinMode(24, GPIO.OUTPUT);
    GPIO.pinMode(27, GPIO.OUTPUT);

    //ARDUINO
    //println (Serial.list());
    String portName = Serial.list()[2];
    myPort = new Serial(this, portName, 9600);
  } else if (id.equals("0")) {
    // test, from other computer
  } else {
    // in the library, only 1 tree
    GPIO.pinMode(23, GPIO.OUTPUT);
    motorPin = 23;

    //ARDUINO
    //println (Serial.list());
    try {
      String portName = Serial.list()[2];
      myPort = new Serial(this, portName, 9600);
    }
    catch (Exception e)
    {
      println(e);
    }
  }

  motorOff(motorPin);
}

void draw() {
  background(0);

  if ( myPort.available() > 0) {  // If data is available,
    val = myPort.read();         // read it and store it in val
    print(val);
    valToString = str(val);
    wsc.sendMessage(valToString);
  }
}


void webSocketEvent(String msg) {
  println(msg);
  if (isShaking == false)
  {
    if ( msg.charAt(0) == 'b')
    {
      if (msg.charAt(1)==(id.charAt(0)))
      {
        motorOn(motorPin);
        delay(1000);
        motorOff(motorPin);
      }
    }
  }
}

public void motorOn(int pin)
{
  isShaking = true;
  GPIO.digitalWrite(pin, GPIO.HIGH);
}

public void motorOff(int pin)
{
  isShaking = false;
  GPIO.digitalWrite(pin, GPIO.LOW);
}