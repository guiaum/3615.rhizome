# -*- coding: utf-8 -*-

# Définition d'un client réseau gérant en parallèle l'émission
# et la réception des messages (utilisation de 2 THREADS).


import socket, sys, threading
import RPi.GPIO as GPIO
import time

host = '192.168.1.10'
port = 46000

# Pin Definitons:
ledPin = 13 # Broadcom pin 23 (P1 pin 16)
butPin = 5 # Broadcom pin 17 (P1 pin 11)

# Pin Setup:
GPIO.setmode(GPIO.BOARD) # Broadcom pin-numbering scheme
GPIO.setup(ledPin, GPIO.OUT) # LED pin set as output
GPIO.setup(butPin, GPIO.IN, pull_up_down=GPIO.PUD_UP) # Button pin set as input w/ pull-up


class ThreadReception(threading.Thread):
    """objet thread gérant la réception des messages"""
    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn           
    # réf. du socket de connexion
    def run(self):
        while 1:
            message_recu = self.connexion.recv(1024).decode("Utf8")
            print("*" + message_recu + "*")
            if (message_recu == "button pressed\n"):
                GPIO.output(ledPin, GPIO.HIGH)
                time.sleep(5)
                GPIO.output(ledPin, GPIO.LOW)
            if not message_recu or message_recu.upper() =="FIN\n":
                break
# Le thread <réception> se termine ici.

#On force la fermeture du thread <émission> :
        th_E._stop()
        print("Client arrêté. Connexion interrompue.")
        self.connexion.close()

class ThreadEmission(threading.Thread):
    """objet thread gérant l'émission des messages"""
    def __init__(self, conn):
        threading.Thread.__init__(self)
        self.connexion = conn           
# réf. du socket de connexion
    def run(self):
        try:
            while 1:
                if GPIO.input(butPin) == False: # button is released
                    message_emis = "button pressed\n"
                    self.connexion.send(message_emis)
                    time.sleep(2)
                else:
                    # TROUVER UN TRUC QUI FAIT RIEN, genre un print
        except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
            GPIO.cleanup() # cleanup all GPIO
        # message_emis = sys.stdin.readline()
        # self.connexion.send(message_emis)

# Programme principal - Établissement de la connexion :
connexion = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

try:

    connexion.connect((host, port))

except socket.error:
    print("La connexion a échoué.")
    sys.exit()
print("Connexion établie avec le serveur.")
# Dialogue avec le serveur : on lance deux threads pour gérer
# indépendamment l'émission et la réception des messages :
th_E = ThreadEmission(connexion)
th_R = ThreadReception(connexion)
th_E.start()
th_R.start()